
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.sitemaps.views import sitemap


admin.autodiscover()
admin.site.enable_nav_sidebar = False

from _app.sitemaps import PakeSitemaps

sitemaps = {
    'story': PakeSitemaps,
}


urlpatterns = [
    path('admin/', admin.site.urls),
    path('ckeditor', include('ckeditor_uploader.urls')),
    path('sitemap.xml', sitemap, {'sitemaps': sitemaps},
                            name='django.contrib.sitemaps.views.sitemap'),

    path('', include('_app.urls')),
]

handler404 = '_app.views.error_404'

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    