from django.contrib import admin
from .models import (
   Kontak, Paket_tour, Paket_transport, 
   Pesanan_paket, Testimony, Blog,
   Kategory, Banner, Populer, Detail_harga, Galeri
)
# Register your models here.

# slug

class PaketAdmin(admin.ModelAdmin):
    readonly_fields = ['slug',]



admin.site.register(Paket_tour, PaketAdmin)
admin.site.register(Paket_transport)
admin.site.register(Pesanan_paket)
admin.site.register(Kontak)
admin.site.register(Testimony)
admin.site.register(Blog)
admin.site.register(Kategory)
admin.site.register(Banner)
admin.site.register(Populer)
admin.site.register(Detail_harga)
admin.site.register(Galeri)
# admin.site.register(List_tour)
# admin.site.register(Detail_harga)