from distutils.command.upload import upload
from email.policy import default
from statistics import mode
from django.db import models
from django.db.models.fields import EmailField
from django.utils.text import slugify
# from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField
from django.urls import reverse




KATEGORY_PILIHAN = (
    ('Honeymoon Lombok','Honeymoon Lombok'),
    ('Tour Lombok','Tour Lombok'),
    ('Harian Lombok','Harian Lombok')
)

class Kategory(models.Model):
    nama = models.CharField(max_length=200, choices=KATEGORY_PILIHAN, default='')
    def __str__(self):
        return self.nama


class Paket_tour(models.Model):
    nama_paket = models.CharField(max_length=200, default="")
    Kategory = models.ForeignKey(Kategory, related_name='paket_tour', default="", on_delete=models.CASCADE)
    deskripsi = models.TextField()
    poto       = models.ImageField(upload_to='paket_tour', default='')  
    durasi     = models.CharField(max_length=200)
    harga      = models.CharField(max_length=200)
    itenerary = RichTextUploadingField()
    slug = models.SlugField(blank=True, null=True, editable=False)

    def __str__(self):
        return self.nama_paket 
    
    def save(self, *args, **kwargs):
        self.slug = slugify(self.nama_paket)
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('_app:detail', args={self.slug})

class Detail_harga(models.Model):
    paket = models.ForeignKey(Paket_tour, related_name='detail_harga', on_delete=models.CASCADE)
    jumlah = models.CharField(max_length=200)
    tour = models.CharField(max_length=100)
    tour_hotel = models.CharField(max_length=100)   

    def __str__(self):
        return self.jumlah


class Populer(models.Model):
    nama = models.CharField(max_length=200)
    poto       = models.ImageField(upload_to='populer', default='')  
    def __str__(self):
        return self.nama


class Paket_transport(models.Model):
    nama_tujuan = models.CharField(max_length=200) 
    harga       = models.IntegerField()

    def __str__(self):
        return self.nama_tujuan



class Testimony(models.Model):
    nama = models.CharField(max_length=100)
    poto = models.ImageField(upload_to="userTestimony", default='')
    komentar = models.TextField()


    def __str__(self):
        return self.nama


class Blog(models.Model):
    judul = models.CharField(max_length=100)
    content = models.TextField()
    penulis = models.CharField(max_length=100, default="")    

class Pesanan_paket(models.Model):
    nama          = models.CharField(max_length=200)
    alamat        = models.CharField(max_length=200)
    nomor_hp      = models.CharField(max_length=200)
    jumlah_orang  = models.CharField(max_length=100)
    tanggal_jemput= models.DateField()
    paket         = models.CharField(max_length=200)


    def __str__(self):
        return self.nama


class Galeri(models.Model):
    judul = models.CharField(max_length=200)
    poto = models.ImageField(upload_to='galeri', default='')

    def __str__(self):
        return self.judul


class Kontak(models.Model):
    nama  = models.CharField(max_length=100) 
    email = models.EmailField()
    pesan = models.TextField()

    def __str__(self):
        return self.nama


class Banner(models.Model):
    title = models.CharField(max_length=100)
    image = models.ImageField(upload_to="banner", default='')

    def __str__(self):
        return self.title
    

        

        