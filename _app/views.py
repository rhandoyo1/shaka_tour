from django.db.models.fields import EmailField
from django.shortcuts import render,redirect
from django.contrib import messages
from .models import ( 
        Paket_transport, Paket_tour, Pesanan_paket, Kontak, Pesanan_paket, 
        Banner, Populer,Testimony, Kategory
    )

def home(request):
    paket_tour = Paket_tour.objects.all()
    banners = Banner.objects.all()
    populers = Populer.objects.all()
    testimony = Testimony.objects.all()
    kategory = Kategory.objects.all()

    context = {
        'title':'Agen Wisata Lombok | Home',
        'paket_tours': paket_tour,
        'banners':banners,
        'populers':populers,
        'testy':testimony,
        'kategory':kategory
    }

    if(request.method == 'POST'):
        id_kategory = request.POST.get('ktr')
        context['paket_tours'] = Paket_tour.objects.filter(Kategory=id_kategory)
        return render(request,'home.html', context)   
    else:
        context['paket_tours'] = Paket_tour.objects.all()

    return render(request,"home.html", context)




def detail(request, slug):
    paket = Paket_tour.objects.get(slug=slug)
    paket_other = Paket_tour.objects.exclude(slug=slug)


    context = {
        'title':'Detail',
        'paket': paket,
        'paket_other': paket_other
    }

    if request.method == 'POST':
        Pesanan_paket.objects.create(
            nama            = request.POST.get('nama'),
            alamat          = request.POST.get('alamat'),
            nomor_hp        = request.POST.get('nomor_hp'),
            jumlah_orang    = request.POST.get('jumlah_orang'),
            tanggal_jemput  = request.POST.get('tanggal_penjemputan'),
            paket           = paket.nama_paket
        )

        Pesanan_paket.save
        messages.success(request,'Pemesanan paket sukses, kami akan menghubungi anda via wa')

        return redirect('_app:index')
    return render(request,'detail.html',context)

def kontak(request):
    context = {
        'title':'Kontak'
    }

    if request.method == 'POST':
        nama  = request.POST.get('nama')
        email = request.POST.get('email')
        pesan = request.POST.get('pesan')

        Kontak.objects.create(
            nama = nama,
            email =email,
            pesan = pesan
        )
        Kontak.save
        messages.success(request,'Terimakasih, pesan anda sudah terkirim')

    else:
        return render(request,'kontak.html',context)    

    return render(request,'kontak.html',context)


def paket(request):
    return render(request,'paket.html')

def transport(request):
    paket_transports = Paket_transport.objects.all()
    context = {
        'title':'transport',
        'paket_transport': paket_transports
    }

    return render(request,'transport.html', context)


def tentang_kami(request):

    return render(request,'tentang_kami.html',context={'title':'Tentang kami'})
    

def galeri(request):

    return render(request,'galeri.html',context={'title':'Gallery'})


def error_404(request, exception):
    return render(request,'404.html')





