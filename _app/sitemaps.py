from django.contrib.sitemaps import Sitemap
from .models import Paket_tour


class PakeSitemaps(Sitemap):
    changefreq = "monthly"
    priority = 0.5

    def items(self):
        return Paket_tour.objects.all()